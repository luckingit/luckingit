//
//  IndexOrderChooseView.m
//  hs
//
//  Created by RGZ on 16/4/11.
//  Copyright © 2016年 luckin. All rights reserved.
//

#import "IndexOrderChooseView.h"

@implementation IndexOrderChooseView
{
    NSTimer *_longTimer;
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.minusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.minusButton.frame = CGRectMake(0, 0, self.frame.size.height/5*4, self.frame.size.height);
        [self.minusButton setTitle:@"—" forState:UIControlStateNormal];
        self.minusButton.titleLabel.font = [UIFont systemFontOfSize:10];
        [self.minusButton setTitleColor:Color_Gold forState:UIControlStateNormal];
        [self.minusButton addTarget:self action:@selector(minusClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.minusButton];
        
        self.minusLineView = [[UIView alloc]initWithFrame:CGRectMake(self.minusButton.frame.size.width, 0, 0.5, self.frame.size.height)];
        self.minusLineView.backgroundColor = Color_gray;
        [self addSubview:self.minusLineView];
        
        self.plusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.plusButton.frame = CGRectMake(self.frame.size.width - self.frame.size.height/5*4, 0, self.frame.size.height/5*4, self.frame.size.height);
        [self.plusButton setTitle:@"+" forState:UIControlStateNormal];
        self.plusButton.titleLabel.font = [UIFont systemFontOfSize:18];
        [self.plusButton setTitleColor:Color_Gold forState:UIControlStateNormal];
        [self.plusButton addTarget:self action:@selector(plusClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.plusButton];
        
        self.plusLineView = [[UIView alloc]initWithFrame:CGRectMake(self.plusButton.frame.origin.x - 0.5, 0, 0.5, self.frame.size.height)];
        self.plusLineView.backgroundColor = Color_gray;
        [self addSubview:self.plusLineView];
        
        UILongPressGestureRecognizer *minusLongGes = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(minusLongClick:)];
        minusLongGes.minimumPressDuration = 0.5;
        [self.minusButton addGestureRecognizer:minusLongGes];
        
        UILongPressGestureRecognizer *plusLongGes = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(plusLongClick:)];
        plusLongGes.minimumPressDuration = 0.5;
        [self.plusButton addGestureRecognizer:plusLongGes];
        
        self.textField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.minusLineView.frame), 0, self.frame.size.width - CGRectGetMaxX(self.minusLineView.frame) - (self.frame.size.width - self.plusLineView.frame.origin.x), self.frame.size.height)];
        self.textField.textAlignment = NSTextAlignmentCenter;
        self.textField.text = @"1";
        self.textField.font = [UIFont systemFontOfSize:15];
        self.textField.textColor = [UIColor blackColor];
        self.textField.delegate = self;
        self.textField.keyboardType = UIKeyboardTypeDecimalPad;
        self.textField.returnKeyType = UIReturnKeyDone;
        [self addSubview:self.textField];
        
        self.jump = 1;
    }
    return self;
}

-(void)closeTimer{
    [_longTimer invalidate];
    _longTimer = nil;
}

-(void)minusClick{
    //    NSArray *array = [self.textField.text componentsSeparatedByString:@"."];
    //    NSString *str = @"";
    //    if (array.count == 2) {
    //        str = [NSString stringWithFormat:@".%@",array[1]];
    //    }
    //
    //    self.textField.text = [NSString stringWithFormat:@"%d%@",[array[0] intValue] - (int)self.jump , str];
    //
    //    if ([self.textField.text floatValue] < 0) {
    //        self.textField.text = @"0";
    //    }
    [self minusLong];
    self.minusBlock(self);
}

-(void)plusClick{
    //    NSArray *array = [self.textField.text componentsSeparatedByString:@"."];
    //    NSString *str = @"";
    //    if (array.count == 2) {
    //        str = [NSString stringWithFormat:@".%@",array[1]];
    //    }
    //
    //    self.textField.text = [NSString stringWithFormat:@"%d%@",[array[0] intValue] + (int)self.jump , str];
    
    [self plusLong];
    self.plusBlock(self);
}

-(void)minusLongClick:(UIGestureRecognizer *)longGes{
    
    
    switch (longGes.state) {
        case UIGestureRecognizerStateBegan:
        {
            _longTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(minusLong) userInfo:nil repeats:YES];
        }
            break;
            
            
        case UIGestureRecognizerStateCancelled:
        {
            [self closeTimer];
            self.minusBlock(self);
        }
            break;
            
        case UIGestureRecognizerStateEnded:
        {
            [self closeTimer];
            self.minusBlock(self);
        }
            break;
            
        default:
            break;
    }
}

-(void)plusLongClick:(UIGestureRecognizer *)longGes{
    
    switch (longGes.state) {
        case UIGestureRecognizerStateBegan:
        {
            _longTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(plusLong) userInfo:nil repeats:YES];
        }
            break;
            
            
        case UIGestureRecognizerStateCancelled:
        {
            [self closeTimer];
            self.plusBlock(self);
        }
            break;
            
        case UIGestureRecognizerStateEnded:
        {
            [self closeTimer];
            self.plusBlock(self);
        }
            break;
            
        default:
            break;
    }
    
}

-(void)minusLong{
    NSString    *pointLength = @"";
    NSArray *textFieldArray = [self.textField.text componentsSeparatedByString:@"."];
    if (textFieldArray.count >= 2) {
        pointLength = textFieldArray[1];
    }
    NSArray *jumpArray = [[NSString stringWithFormat:@"%f",self.jump] componentsSeparatedByString:@"."];
    if (jumpArray.count >= 2) {
        if ([jumpArray[1] intValue] > 0) {
            self.textField.text = [DataUsedEngine conversionFloatNum:[self.textField.text floatValue] - self.jump  ExpectFloatNum:(int)[pointLength length]];
        }
        else{
            [self minusExecute];
        }
    }
    else{
        [self minusExecute];
    }
    
    
    if ([self.textField.text floatValue] < 0) {
        self.textField.text = @"0";
    }
}

-(void)minusExecute{
    NSArray *array = [self.textField.text componentsSeparatedByString:@"."];
    NSString *str = @"";
    if (array.count == 2) {
        str = [NSString stringWithFormat:@".%@",array[1]];
    }
    
    self.textField.text = [NSString stringWithFormat:@"%.0f%@",[array[0] intValue] - self.jump , str];
}

-(void)plusLong{
    NSString    *pointLength = @"";
    NSArray *textFieldArray = [self.textField.text componentsSeparatedByString:@"."];
    if (textFieldArray.count >= 2) {
        pointLength = textFieldArray[1];
    }
    NSArray *jumpArray = [[NSString stringWithFormat:@"%f",self.jump] componentsSeparatedByString:@"."];
    if (jumpArray.count >= 2) {
        if ([jumpArray[1] intValue] > 0) {
            self.textField.text = [DataUsedEngine conversionFloatNum:[self.textField.text floatValue] + self.jump ExpectFloatNum:(int)[pointLength length]];
        }
        else{
            [self plusExecute];
        }
    }
    else{
        [self plusExecute];
    }
}

-(void)plusExecute{
    NSArray *array = [self.textField.text componentsSeparatedByString:@"."];
    NSString *str = @"";
    if (array.count == 2) {
        str = [NSString stringWithFormat:@".%@",array[1]];
    }
    
    self.textField.text = [NSString stringWithFormat:@"%.0f%@",[array[0] intValue] + self.jump , str];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [textField performSelector:@selector(selectAll:) withObject:textField afterDelay:0];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    self.editEndBlock(self);
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if ([textField.text rangeOfString:@"."].location != NSNotFound && [string isEqualToString:@"."]) {
        textField.text = [textField.text substringToIndex:textField.text.length];
        return NO;
    }
    return YES;
    
}


-(void)plusFire{
    [self plusClick];
}

-(void)minusFire{
    [self minusClick];
}


@end
