//
//  IndexOrderModel.m
//  hs
//
//  Created by RGZ on 16/4/25.
//  Copyright © 2016年 luckin. All rights reserved.
//

#import "IndexOrderModel.h"
#import "IndexViewController.h"

@implementation IndexOrderModel
{
    NSTimer         *_timer;//轮询下单是否成功
    int             time;//倒计时5秒
    BOOL            _isOpenQuick;
}
DEF_SINGLETON(IndexOrderModel)


+(instancetype)defaultModel{
    return [IndexOrderModel sharedInstance];
}

-(void)orderBeginDic:(IndexBuyModel *)indexBuyModel
            NumCount:(NSString *)numCount
            BuyState:(int)buyState
           SelectGet:(int)selectGet
       CouponIDArray:(NSMutableArray *)couPonIDArray
        CouPonSelect:(BOOL)isCouPonSelect
       IntegralTrade:(TradeConfigerModel *)integralTrade
          MoneyTrade:(TradeConfigerModel *)moneyTrade
          IsIntegral:(BOOL)isIntegral
       ChooseStopNum:(int)chooseStopNum{
    
    
    [UIEngine sharedInstance].progressStyle = 2;
    [[UIEngine sharedInstance] showProgress];
    [DataEngine requestToGetSystemDateWithComplete:^(BOOL SUCCESS, NSString *data , NSString *timeInterval) {
        NSString *systemTime = @"";
        if (SUCCESS) {
            systemTime = data;
        }
        else{
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            systemTime = [dateFormatter stringFromDate:[NSDate date]];
        }
        TradeConfigerModel *tradeModel;
        if (isIntegral) {
            if (chooseStopNum == 0) {
                tradeModel = integralTrade;
            }
        }
        else{
            if (chooseStopNum == 0) {
                tradeModel = moneyTrade;
            }
        }
        
        NSString *tradeID = @"";
        if (tradeModel == nil || [tradeModel isKindOfClass:[NSNull class]]) {
            tradeID = @"0";
        }
        else{
            tradeID = tradeModel.tradeID;
        }
        
        NSDictionary *dic = [NSDictionary dictionary];
        //判断是否有选择使用优惠券。如果使用了。就新增加一个字段。
        if (isCouPonSelect == YES)
        {
            dic = @{
                    @"tradeType": [NSNumber numberWithInt:buyState], //看多看空
                    @"userBuyDate": systemTime,              //购买时间
                    @"userBuyPrice": indexBuyModel.currentPrice, //价格
                    
                    @"rate":[NSNumber numberWithDouble:[tradeModel.rate doubleValue]],
                    @"futuresCode": indexBuyModel.code,      //代码
                    @"count": numCount,            //手数
                    @"traderId": tradeID,                 //配资ID
                    @"stopProfit":[tradeModel.maxProfit componentsSeparatedByString:@","][selectGet],//止盈金额
                    @"couponIds":couPonIDArray,
                    };
            
        }else
        {
            dic = @{
                    @"tradeType": [NSNumber numberWithInt:buyState], //看多看空
                    @"userBuyDate": systemTime,              //购买时间
                    @"userBuyPrice": indexBuyModel.currentPrice, //价格
                    
                    @"rate":[NSNumber numberWithDouble:[tradeModel.rate doubleValue]],
                    @"futuresCode": indexBuyModel.code,      //代码
                    @"count": numCount,            //手数
                    @"traderId": tradeID,                 //配资ID
                    @"stopProfit":[tradeModel.maxProfit componentsSeparatedByString:@","][selectGet],//止盈金额
                    };
        }
        
        self.orderBeginBlock(dic);
    }];
    
}

-(void)orderGoExecute:(NSDictionary *)aPramDic IsOpenQuick:(BOOL)isOpenQuick IsIntegral:(BOOL)isIntegral ProductModel:(FoyerProductModel *)productModel IndexOrderController:(IndexOrderController *)indexOrderController{
    _isOpenQuick = isOpenQuick;
    NSString *jsonStr = [self toJSON:aPramDic];
    NSDictionary *jsonDic = @{
                              @"orderData" : jsonStr,
                              @"version" : @"0.0.3" ,
                              @"token" : [[CMStoreManager sharedInstance] getUserToken]
                              };
    [DataEngine requestToBuy:jsonDic completeBlock:^(BOOL SUCCESS, NSDictionary *dictionary) {
        if (SUCCESS) {
            _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(afterTime) userInfo:nil repeats:YES];
            [self searchOrderStork:dictionary[@"data"][@"futuredOrderIdsStr"] ProductModel:productModel IsOpenQuick:isOpenQuick IndexOrderController:indexOrderController];
        }
        //抱歉，目前暂不支持现金交易！  注：在现金大厅不会出现
        else if ([dictionary[@"code"] intValue] == 49999 || [dictionary[@"code"] intValue] == 49997){
            if (isOpenQuick) {
                [[UIEngine sharedInstance] showAlertWithTitle:@"抱歉，目前暂不支持现金交易！" ButtonNumber:1 FirstButtonTitle:@"确定" SecondButtonTitle:@""];
                [UIEngine sharedInstance].alertClick = ^(int aIndex){
                    self.cantTradeBlock();
                    [[UIEngine sharedInstance] hideProgress];
                };
            }
            else{
                [[UIEngine sharedInstance] showAlertWithTitle:@"抱歉，目前暂不支持现金交易！" ButtonNumber:2 FirstButtonTitle:@"确定" SecondButtonTitle:@"返回"];
                [UIEngine sharedInstance].alertClick = ^(int aIndex){
                    if (aIndex == 10087) {
                        self.backSelfBlock();
                    }
                    [[UIEngine sharedInstance] hideProgress];
                };
            }
        }
        //余额不足
        else if([dictionary[@"code"] intValue] == 44007){
            
            if (isOpenQuick) {
                [[UIEngine sharedInstance] showAlertWithTitle:@"您当前现金余额不足，请先充值" ButtonNumber:2 FirstButtonTitle:@"取消" SecondButtonTitle:@"充值"];
                [UIEngine sharedInstance].alertClick = ^(int aIndex){
                    if (aIndex == 10086) {
                        
                    }
                    if (aIndex == 10087) {
                        self.accountBlock();
                    }
                };
            }
            else{
                if (isIntegral == 0) {
                    [[UIEngine sharedInstance] showAlertWithTitle:@"您当前现金余额不足，请先充值" ButtonNumber:2 FirstButtonTitle:@"返回" SecondButtonTitle:@"充值"];
                    [UIEngine sharedInstance].alertClick = ^(int aIndex){
                        if (aIndex == 10086) {
                            self.backSelfBlock();
                        }
                        if (aIndex == 10087) {
                            self.accountBlock();
                        }
                    };
                }
                else{
                    [[UIEngine sharedInstance] showAlertWithTitle:@"您当前积分余额不足，无法买入" ButtonNumber:1 FirstButtonTitle:@"确定" SecondButtonTitle:@""];
                    [UIEngine sharedInstance].alertClick = ^(int aIndex){
                        
                    };
                }
            }
            [[UIEngine sharedInstance] hideProgress];
        }
        else if([dictionary[@"code"] intValue] == 44026){
            //44026 涨跌幅过滤
            [[UIEngine sharedInstance] showAlertWithTitle:@"申报失败，涨跌幅过滤" ButtonNumber:1 FirstButtonTitle:@"确定" SecondButtonTitle:@""];
            [UIEngine sharedInstance].alertClick = ^(int aIndex){
                
            };
            [[UIEngine sharedInstance] hideProgress];
        }
        //用户提交买入价格为0或空、行情波动大于等于4个点
        else if ([dictionary[@"code"] intValue] == 44031 || [dictionary[@"code"] intValue] == 44032){
            if (dictionary[@"msg"] != nil && ![dictionary[@"msg"] isKindOfClass:[NSNull class]]) {
                NSArray *messageInfo = [dictionary[@"msg"] componentsSeparatedByString:@"|"];
                if (messageInfo.count > 1) {
                    [[UIEngine sharedInstance] showAlertWithOrderWithTitle:messageInfo[0] Message:messageInfo[1]];
                    [UIEngine sharedInstance].alertClick = ^(int aIndex){};
                }
            }
            [[UIEngine sharedInstance] hideProgress];
        }
        else {
            NSString *msg = @"";
            if (dictionary == nil || dictionary[@"msg"] == nil) {
                msg = @"申报失败";
            }
            else{
                msg = dictionary[@"msg"];
            }
            [[UIEngine sharedInstance] showAlertWithTitle:[DataUsedEngine nullTrimString:msg] ButtonNumber:2 FirstButtonTitle:@"返回大厅" SecondButtonTitle:@"确定"];
            [UIEngine sharedInstance].alertClick = ^(int aIndex){
                if (aIndex == 10086) {
                    self.closeSocketBlock();
                    
                    if (isOpenQuick) {
                        self.backBlock();
                    }
                    else{
                        self.backRootBlock();
                    }
                }
            };
            [[UIEngine sharedInstance] hideProgress];
        }
    }];
}

#pragma mark - 查询订单交易状态

//查询订单交易状态r
- (void)searchOrderStork:(NSString *)idArray ProductModel:(FoyerProductModel *)productModel IsOpenQuick:(BOOL)isOpenQuick IndexOrderController:(IndexOrderController *)indexOrderController
{
    if (_timer == nil) {
        return;
    }else{
        int fundType = 0;
        if (![productModel.loddyType isEqualToString:@"1"]) {
            fundType = 1;
        }
        
        NSDictionary *dic = @{
                              @"futuredOrderIdsStr" : idArray,
                              @"version" : VERSION ,
                              @"token" : [[CMStoreManager sharedInstance] getUserToken],
                              @"fundType":[NSNumber numberWithInt:fundType],
                              };
        
        NSString *urlStr = [NSString stringWithFormat:@"%@/order/futures/orderStatus", K_MGLASS_URL];
        
        [NetRequest postRequestWithNSDictionary:dic
                                            url:urlStr
                                   successBlock:^(NSDictionary *dictionary) {
                                       
                                       if ([dictionary[@"code"] intValue] == 200) {
                                           
                                           //                                   data:
                                           //                                       -2：买入失败
                                           //                                       0：买待处理，创建订单默认状态
                                           //                                       1：买处理中
                                           //                                       2：买委托成功，即申报成功
                                           //                                       3：持仓中
                                           //                                       4：卖处理中
                                           //                                       5：卖委托成功，即申报成功
                                           //                                       6：卖出成功
                                           
                                           int count = 0;
                                           for (int i = 0; i < [dictionary[@"data"] count]; i++) {
                                               if ([dictionary[@"data"][i][@"status"] floatValue] == 2 || [dictionary[@"data"][i][@"status"] floatValue] == 3) {
                                                   [_timer invalidate];
                                                   _timer = nil;
                                                   [[UIEngine sharedInstance] hideProgress];
                                                   
                                                   /**
                                                    *  下单成功刷新持仓收益
                                                    */
                                                   [[NSNotificationCenter defaultCenter] postNotificationName:kOrderSuccess object:nil];
                                                   
                                                   [[UIEngine sharedInstance] showAlertWithTitle:@"已申报成功" ButtonNumber:1 FirstButtonTitle:@"确定" SecondButtonTitle:@""];
                                                   [UIEngine sharedInstance].alertClick = ^(int aIndex){
                                                       [[NSNotificationCenter defaultCenter] postNotificationName:kSegValueChange object:nil];
                                                       UIViewController *viewController = nil;
                                                       for (UIViewController *indexViewController in indexOrderController.navigationController.viewControllers) {
                                                           if ([indexViewController isKindOfClass:[IndexViewController class]]) {
                                                               viewController = indexViewController;
                                                           }
                                                       }
                                                       self.backSelfBlock();
                                                   };
                                                   break;
                                               }
                                               else if([dictionary[@"data"][i][@"status"] floatValue] == 0 || [dictionary[@"data"][i][@"status"] floatValue] == 1){
                                                   count ++;
                                                   if (count == [dictionary[@"data"] count]) {
                                                       [self searchOrderStork:idArray ProductModel:productModel IsOpenQuick:isOpenQuick IndexOrderController:indexOrderController];
                                                   }
                                               }
                                               else{
                                                   
                                                   [_timer invalidate];
                                                   _timer = nil;
                                                   [[UIEngine sharedInstance] hideProgress];
                                                   
                                                   [[UIEngine sharedInstance] showAlertWithTitle:@"申报失败" ButtonNumber:2 FirstButtonTitle:@"返回大厅" SecondButtonTitle:@"确定"];
                                                   [UIEngine sharedInstance].alertClick = ^(int aIndex){
                                                       if (aIndex == 10086) {
                                                           self.closeSocketBlock();
                                                           if (isOpenQuick) {
                                                               self.backBlock();
                                                           }
                                                           else{
                                                               self.backRootBlock();
                                                           }
                                                       }
                                                   };
                                                   
                                                   
                                               }
                                           }
                                           
                                       }
                                       else{
                                           [self searchOrderStork:idArray ProductModel:productModel IsOpenQuick:isOpenQuick IndexOrderController:indexOrderController];
                                       }
                                       
                                   }
                                   failureBlock:^(NSError *error){
                                       [self searchOrderStork:idArray ProductModel:productModel IsOpenQuick:isOpenQuick IndexOrderController:indexOrderController];
                                       
                                   }];
    }
}

-(void)afterTime{
    time ++;
    if (time > 5) {
        time = 0;
        [_timer invalidate];
        _timer = nil;
        [[UIEngine sharedInstance] hideProgress];
        self.backRootBlock();
        
        if(!_isOpenQuick){
            self.closeSocketBlock();
        }
        else{
            self.backBlock();
        }
    }
}

-(NSString *)toJSON:(id)aParam
{
    NSData   *jsonData=[NSJSONSerialization dataWithJSONObject:aParam options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonStr;
}

-(NSString *)getChooseButtonTitleWithBuyState:(int)aBuyState{
    NSLog(@"%@,%@,%@,%@",self.resultDirection,self.resultPrice,[IndexOrderModel sharedInstance].resultDirection,[IndexOrderModel sharedInstance].resultPrice);
    if (![DataUsedEngine nullTrim:self.resultDirection] && ![self.resultDirection isEqualToString:@""] && ![DataUsedEngine nullTrim:self.resultPrice] && ![self.resultPrice isEqualToString:@""]) {
        return @"请设置委托条件";
    }
    
    NSString *resultStr = @"";
    
    if (aBuyState == 0) {//看多
        resultStr = [resultStr stringByAppendingString:@"看多价"];
    }
    else if (aBuyState == 1){//看空
        resultStr = [resultStr stringByAppendingString:@"看空价"];
    }
    
    if ([self.resultDirection isEqualToString:@"1"]) {//<=
        resultStr = [resultStr stringByAppendingString:@"≤"];
    }
    else if ([self.resultDirection isEqualToString:@"2"]){//>=
        resultStr = [resultStr stringByAppendingString:@"≥"];
    }
    
    resultStr = [resultStr stringByAppendingString:self.resultPrice];
    
    return resultStr;
}

#pragma mark 条件单下单

-(void)chooseOrderGoExecute:(NSDictionary *)aParamDic{
    
    [DataEngine requestToGetConditionOrderWithParamDic:aParamDic completeBlock:^(id data) {
        if ([DataUsedEngine nullTrim:data[@"code"]] && [data[@"code"] floatValue] == 200) {
            [[UIEngine sharedInstance] showAlertWithType:AlertConditionOrderSuccess ButtonNum:1 LeftTitle:@"确定" RightTitle:@""];
            [UIEngine sharedInstance].orderContidionBlock = ^(){//查看委托单
                self.contidionOrderBlock();
            };
            [UIEngine sharedInstance].alertClick = ^(int aIndex){//回行情
                self.backSelfBlock();
            };
        }
        else{
            [[UIEngine sharedInstance] showAlertWithTitle:data[@"msg"] ButtonNumber:1 FirstButtonTitle:@"确定" SecondButtonTitle:nil];
            [UIEngine sharedInstance].alertClick = ^(int aIndex){
                
            };
        }
    }];
    
    
}

@end
