//
//  IPSUpmpViewController.h
//  UPMP
//
//  Created by IH1246 on 15/10/16.
//  Copyright © 2015年 IH1246. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol IPSUPMPDelegate;
@interface IPSUpmpViewController : UIViewController
@property(nonatomic,strong)NSString* merCode;
@property(nonatomic,strong)NSString* accCode;
@property(nonatomic,strong)NSString* merBillNo;
@property(nonatomic,strong)NSString* ccyCode;
@property(nonatomic,strong)NSString* tranAmt;
@property(nonatomic,strong)NSString* requestTime;
@property(nonatomic,strong)NSString* ordPerVal;
@property(nonatomic,strong)NSString* orderEncodeType;
@property(nonatomic,strong)NSString* mobileDeviceType;
@property(nonatomic,strong)NSString* sign;
@property(nonatomic,strong)NSString* merNoticeUrl;
@property(nonatomic,strong)NSString* orderDesc;
@property (weak, nonatomic) id<IPSUPMPDelegate> delegate;
@property(nonatomic,strong) UIColor * originalNavigationBarcolor;//导航栏颜色
@property(nonatomic,strong) NSString * banktn;
+(void)IPSStartPayWithMerCode:(NSString*)merCode
                      accCode:(NSString*)accCode
                    merBillNo:(NSString*)merBillNo
                      ccyCode:(NSString*)ccyCode
                      tranAmt:(NSString*)tranAmt
                  requestTime:(NSString*)requestTime
                    ordPerVal:(NSString*)ordPerVal
              orderEncodeType:(NSString*)orderEncodeType
                    orderDesc:(NSString*)orderDesc
             mobileDeviceType:(NSString*)mobileDeviceType
                         sign:(NSString*)sign
                 merNoticeUrl:(NSString*)merNoticeUrl
                     delegate:(id<IPSUPMPDelegate>)delegate
               viewController:(UIViewController*)selfViewController;

@end


@protocol IPSUPMPDelegate <NSObject>
@optional
-(void)orderCompletedStatus:(NSString*)status
                    merCode:(NSString*)merCode
                    accCode:(NSString*)accCode
                  merBillNo:(NSString*)merBillNo
                    ccyCode:(NSString*)ccyCode
                    tranAmt:(NSString*)tranAmt
                requestTime:(NSString*)requestTime
                  orderDesc:(NSString*)orderDesc
           mobileDeviceType:(NSString*)mobileDeviceType
               merNoticeUrl:(NSString*)merNoticeUrl;

@end