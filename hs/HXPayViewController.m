//
//  HXPayViewController.m
//  hs
//
//  Created by RGZ on 16/5/17.
//  Copyright © 2016年 luckin. All rights reserved.
//

#import "HXPayViewController.h"
#import "GTMBase64.h"
#import <IPSUPMPSDK/IPSUpmpViewController.h>

#if TARGET_IPHONE_SIMULATOR
#define SIMULATOR 1
#elif TARGET_OS_IPHONE
#define SIMULATOR 0
#endif

@interface HXPayViewController ()<IPSUPMPDelegate>
{
    int     appearNum;
}
@property (nonatomic,strong)NSString    *merCode;
@property (nonatomic,strong)NSString    *accCode;
@property (nonatomic,strong)NSString    *merBillNo;
@property (nonatomic,strong)NSString    *tranAmt;
@property (nonatomic,strong)NSString    *ordPerVal;
@property (nonatomic,strong)NSString    *requestTime;
@property (nonatomic,strong)NSString    *orderDesc;
@property (nonatomic,strong)NSString    *merNoticeUrl;
@property (nonatomic,strong)NSString    *bankCard;

@end

@implementation HXPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view.window endEditing:YES];
    
    appearNum = 0;
    
    UILabel *proLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, ScreenHeigth/2 - 40, ScreenWidth, ScreenHeigth)];
    proLabel.text = @"数据加载中...";
    proLabel.textColor = [UIColor lightGrayColor];
    proLabel.textAlignment = NSTextAlignmentCenter;
    proLabel.font = [UIFont systemFontOfSize:42];
    [self.view addSubview:proLabel];
    
    [DataEngine requestToHXPayRecordWithAmt:self.tradeMoney completeBlock:^(NSString *orderID) {
        [self goWithOrderID:orderID];
    }];//后台记录充值点击次数并且获取订单号
    
}

-(void)viewWillAppear:(BOOL)animated{
    appearNum ++;
    if (appearNum > 1) {
        [self.navigationController popViewControllerAnimated:NO];
    }
}

-(void)goWithOrderID:(NSString *)orderID{
#if TARGET_IPHONE_SIMULATOR
    //模拟器
#elif TARGET_OS_IPHONE
    NSString *md5Cert = @"htrdfzBVzuZP03OEdq9RAdODfPlDzGjv4654vvwWsv8TVzntOSYNrZh8DvhufGXGkRzQ3jRQn8yUkn512NABpBfxmeNnfVtGPkpKxJMnSk8ErT7WbDpec1AMK1Pn3ZGL";//按商户号来修改md5证书
    NSString *desKey = @"QMzcSrPjqzQRuP5NSmSlZBmm";//设置des加密的key
    NSString *desIv = @"U40ZUTSK";//设置des加密的向量
    
    
    NSDate * date = [NSDate date];
    NSDateFormatter * dateformatter = [[NSDateFormatter alloc] init];
    dateformatter.dateFormat = @"yyyyMMddHHmmss";
    NSString* dateStr = [NSString stringWithFormat:@"%@", [dateformatter stringFromDate:date]];
    self.merCode = @"180234";//商户号
    self.accCode = @"1802340016";//交易账户号
    self.merBillNo = [NSString stringWithFormat:@"PCDP%@",orderID];;//商户订单号  msg：商户系统生成订单号
    self.tranAmt = self.tradeMoney;//订单金额
    self.ordPerVal = @"1";//订单有效期
    self.requestTime = dateStr;//请求时间
    self.orderDesc = @"充值";//订单描述
    self.merNoticeUrl = [NSString stringWithFormat:@"%@/financy/huanpay/synchReturnNotify",K_MGLASS_URL];
    self.bankCard = @"";
    NSString* bankCardStr = [IPSUpmpViewController TripleDES:self.bankCard encryptOrDecrypt:kCCEncrypt key:desKey desIv:desIv];
    
    NSMutableDictionary* merRequestDict = [[NSMutableDictionary alloc]init];//
    [merRequestDict setValue:self.accCode       forKey:@"accCode"];
    [merRequestDict setValue:self.merBillNo     forKey:@"merBillNo"];
    [merRequestDict setValue:@"156"             forKey:@"ccyCode"];
    [merRequestDict setValue:@"2301"            forKey:@"prdCpde"];
    [merRequestDict setValue:self.tranAmt       forKey:@"tranAmt"];
    [merRequestDict setValue:self.requestTime   forKey:@"requestTime"];
    [merRequestDict setValue:self.ordPerVal     forKey:@"ordPerVal"];
    [merRequestDict setValue:self.merNoticeUrl  forKey:@"merNoticeUrl"];
    [merRequestDict setValue:self.orderDesc     forKey:@"orderDesc"];
    [merRequestDict setValue:bankCardStr        forKey:@"bankCard"];
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:merRequestDict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *requestJason =[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];//
    NSString* merRequestDes = [IPSUpmpViewController TripleDES:requestJason encryptOrDecrypt:kCCEncrypt key:desKey desIv:desIv];//
    NSString* merRequest = [NSString stringWithFormat:@"%@%@%@",self.merCode,merRequestDes,md5Cert];//
    NSString* merRequestMd5 = [IPSUpmpViewController md5:merRequest];//
    [IPSUpmpViewController IPSStartPayWithMerCode:self.merCode
                                             sign:merRequestMd5
                                   merRequestInfo:merRequestDes
                                         delegate:self
                                   viewController:self];
    
#endif
}



-(void)orderCompletedStatus:(NSString*)status
                    merCode:(NSString*)merCode
                  merBillNo:(NSString*)merBillNo
                  orderDesc:(NSString*)orderDesc
                    tranAmt:(NSString*)tranAmt{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
