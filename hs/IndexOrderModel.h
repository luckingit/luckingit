//
//  IndexOrderModel.h
//  hs
//
//  Created by RGZ on 16/4/25.
//  Copyright © 2016年 luckin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TradeConfigerModel.h"
#import "IndexOrderController.h"

typedef void(^OrderBeginBlock)(NSDictionary *dic);
typedef void(^IndexOrderBlock)();
typedef void(^AccountMoneyBlock)();
typedef void(^BackBlock)();
typedef void(^CantTradeBlock)();
typedef void(^CloseSocketBlock)();
typedef void(^ContidionOrderBlock)();

@interface IndexOrderModel : NSObject
AS_SINGLETON(IndexOrderModel)

@property (nonatomic,strong)OrderBeginBlock     orderBeginBlock;
@property (nonatomic,strong)IndexOrderBlock     block;
@property (nonatomic,strong)AccountMoneyBlock   accountBlock;
@property (nonatomic,strong)AccountMoneyBlock   accountSelfBlock;
@property (nonatomic,strong)BackBlock           backBlock;
@property (nonatomic,strong)BackBlock           backSelfBlock;
@property (nonatomic,strong)BackBlock           backRootBlock;
@property (nonatomic,strong)CantTradeBlock      cantTradeBlock;
@property (nonatomic,strong)CloseSocketBlock    closeSocketBlock;
@property (nonatomic,strong)ContidionOrderBlock contidionOrderBlock;
@property (nonatomic,strong)BackBlock           backSelfWithNoAnimation;

@property (nonatomic,strong)NSString            *currentPrice;
@property (nonatomic,strong)NSString            *jumpPrice;

@property (nonatomic,strong)NSString            *inputDirection;//委托条件  1 <=  2 >=
@property (nonatomic,strong)NSString            *inputPrice;//委托价格

@property (nonatomic,strong)NSString            *resultDirection;
@property (nonatomic,strong)NSString            *resultPrice;

-(NSString *)getChooseButtonTitleWithBuyState:(int)aBuyState;

+(instancetype)defaultModel;
/**
 *  Begin and Configer
 *
 *  @param indexBuyModel  indexBuyModel
 *  @param numCount       手数
 *  @param buyState       买多，买空
 *  @param selectGet      触发止盈
 *  @param couPonIDArray  优惠券ID数组
 *  @param isCouPonSelect 是否选中优惠券
 *  @param integralTrade  积分Trade
 *  @param moneyTrade     现金Trade
 *  @param isIntegral     是否积分
 *  @param chooseStopNum  止损index
 */
-(void)orderBeginDic:(IndexBuyModel *)indexBuyModel
                             NumCount:(NSString *)numCount
                             BuyState:(int)buyState
                            SelectGet:(int)selectGet
                        CouponIDArray:(NSMutableArray *)couPonIDArray
                         CouPonSelect:(BOOL)isCouPonSelect
                        IntegralTrade:(TradeConfigerModel *)integralTrade
                            MoneyTrade:(TradeConfigerModel *)moneyTrade
                              IsIntegral:(BOOL)isIntegral
                           ChooseStopNum:(int)chooseStopNum;

/**
 *  Order Go
 *
 *  @param aPramDic             parameter Dictionary
 *  @param isOpenQuick          是否打开快速下单
 *  @param isIntegral           是否积分
 *  @param productModel         productModel
 *  @param indexOrderController self
 */
-(void)orderGoExecute:(NSDictionary *)aPramDic IsOpenQuick:(BOOL)isOpenQuick IsIntegral:(BOOL)isIntegral ProductModel:(FoyerProductModel *)productModel IndexOrderController:(IndexOrderController *)indexOrderController;


-(void)chooseOrderGoExecute:(NSDictionary *)aParamDic;

@end
